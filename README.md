# React-portfolio

I used several of the components that were created in Paul Hanna's tutroial, but I also changed it to make it my own.
I also made it a little more simple, and cut out the resume portion of the portfolio, and upon request will give out my 
resume. I connected several other platforms to my portfolio so that those looking at the portfolio will be able to see my work on those platforms as well. 

If there are any other questions about this. Please feel free to contact me!



# Followed this [YouTube Tutorial](https://www.youtube.com/playlist?list=PL3KAvm6JMiowqFTXj3oPQkhP7aCgRHFTm)
I created a Youtube code along tutorial for this project. Click the link above to view!

# react-portfolio-starter
A React based personal portfolio app using create-react-app and React Router v4.

# Tools
* create-react-app cli
* React MDL material design
* React Router v4

# Start App
Clone repo, install, cd into folder and run:
```git
npm install
npm start
```
